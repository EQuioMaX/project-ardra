# Project Ardra

## Overview

Project Ardra is a hobby Operating System written in C targeting the x86 instruction set.
A rewrite in Rust for RISC-V is currently WIP on branch `risc-v`.

## Roadmap

### Short-term

- [x] VGA Output
- [x] Descriptor Tables
- [x] Interrupt Service Routines
- [x] Keyboard Support
- [x] Physical Memory Manager
- [x] i386 Paging
- [ ] Virtual Memory Manager
- [ ] Heap Allocator
- [ ] VFS Layer
- [ ] ext2 Filesystem Support
- [ ] ELF Loader
- [ ] Dynamic Linker

## Dependencies

* LLVM/Clang & NASM
* Qemu for testing
* Grub/libisoburn/mtools for creating an iso with grub-mkrescue
* GDB for debugging with QEMU's GDBstub

## Test

### i386
`make run`

### AMD64
`make run ARCH=x86_64`

## Debug

`make debug`

`gdb kernel.elf -ex "target remote localhost:1234"`
