ARCH= i386
VPATH= $(KERNELDIR):$(wildcard **/*):$(wildcard **/*[^arch]/*):$(ARCHDIR)

ifeq ($(ARCH), x86_64)
CCARGS= -target $(ARCH)-elf -O0 -Wall -ffreestanding -mno-red-zone -mno-mmx -mno-sse -mno-sse2 -DAMD64 -I $(INCLUDEDIR)
ASARGS= -f elf64 -F dwarf
LDARGS= -melf_x86_64 -T ../$(KERNELDIR)/link.ld


COBJ= kernel.o vga_buffer.o idt.o isr.o pic.o kbd.o bootstrap.o kprintf.o pmm.o
ASOBJ= boot.o gdt.o paging.o interrupts.o

ARCHDIR= ./kernel/arch/x86_64
KERNELDIR= ./kernel
INCLUDEDIR= ./kernel/include

.PHONY= install clean debug run

run: install
	qemu-system-x86_64 -cdrom iso.iso

debug: install
	qemu-system-x86_64 -s -S -cdrom iso.iso

else ifeq ($(ARCH), i386)

CCARGS= -target $(ARCH)-elf -O0 -Wall -ffreestanding -mno-red-zone -mno-mmx -mno-sse -mno-sse2 -DI386 -I $(INCLUDEDIR)
ASARGS= -f elf32 -F dwarf
LDARGS= -melf_i386 -T ../$(KERNELDIR)/link.ld

COBJ= kernel.o bootstrap.o vga_buffer.o gdt.o idt.o isr.o pic.o kbd.o kprintf.o paging.o pmm.o
ASOBJ= boot.o interrupts.o

ARCHDIR= ./kernel/arch/x86
KERNELDIR= ./kernel
INCLUDEDIR= ./kernel/include

.PHONY= install clean debug run

run: install
	@qemu-system-i386 -m 32M -cdrom iso.iso

debug: install
	@qemu-system-i386 -s -S -cdrom iso.iso

endif

kernel: $(COBJ) $(ASOBJ)
	@echo "Building kernel..."
	@mkdir -p build
	@(cd build; ld.lld $(LDARGS) -o kernel.elf $(COBJ) $(ASOBJ))

%.o: %.c
	@echo "Creating C Objects..."
	@mkdir -p build
	@clang $(CCARGS) -g -c -o build/$@ $^

%.o: $(ARCHDIR)/%.asm
	@echo "Creating Assembly Objects..."
	@mkdir -p build
	@nasm $(ASARGS) -g -o build/$@ $<

all: run

install: kernel
	@echo "Creating bootable ISO..."
	@mkdir -p iso/boot/grub/
	@cp build/kernel.elf iso/boot/kernel.elf
	@cp grub.cfg iso/boot/grub/grub.cfg
	@grub-mkrescue -o iso.iso iso

clean:
	@echo "Clearing Objects from previous build..."
	@rm -r ./build
	@rm -rf ./iso *.iso
