#include <mm/pmm.h>

mem_map_entry_t mem_map[20];
uint32_t pmm_memory_size = 0;

//Initializes PMM for extended memory
uint32_t pmm_init() {
    
    pmm_memory_map = (uint32_t*) pmm_mem_map;
    pmm_blocks_max = pmm_memory_size*1024/block_size;
    uint32_t pmm_memory_end = pmm_memory_size * 0x400 - 1;  //Final memory address

    kprintf("[PMM] Kernel's start address: 0x%x\n", (uint32_t)kernel_start_address);
    kprintf("[PMM] Kernel's   end address: 0x%x\n", (uint32_t)kernel_end_address);
    kprintf("[PMM] Last valid memory address: 0x%x\n", pmm_memory_end); //In kB to Bytes
    
    pmm_alloc_region(0, (uint32_t)(kernel_start_address)/block_size);   //Temporarily allocate all of first 1MB
    pmm_alloc_region((uint32_t)kernel_start_address/block_size, (uint32_t)(kernel_end_address - kernel_start_address)/block_size);  // Mark Kernel's location as allocated
    pmm_free_region((uint32_t)kernel_end_address/block_size, (pmm_memory_end - (uint32_t)kernel_end_address)/block_size);   //Free the rest of the address space
    
    uint32_t test = 0;
    pmm_blocks_used = 0;
    
    while(test < pmm_blocks_max) {  //Initialize pmm_blocks_used
        if(pmm_map_test(test)) {
            pmm_blocks_used++;
        } else {
            pmm_blocks_used--;
        }
        test++;
    }
    kprintf("[PMM] Page Frame Allocator initialized!\n");
    kprintf("[PMM] Free blocks: %d\n", pmm_blocks_max - pmm_blocks_used);
    pmm_map_first_free();
    return  pmm_blocks_max;
}

bool pmm_map_allocate_block(uint32_t block_number) {
    
    if (pmm_map_test(block_number)) {
        
        //kprintf("[PMM] Block already allocated\n");
        return false;
    } else {
        
        pmm_map_set(block_number);
        //kprintf("[PMM] Allocated block no.: %d\n", block_number);
        pmm_blocks_used++;
        return true;
    }
}

bool pmm_map_free_block(uint32_t block_number) {
    
    if (!pmm_map_test(block_number)) {
        
        //kprintf("[PMM] Tried to free unallocated block\n");
        pmm_map_unset(block_number);
        return false;
    } else {
        
        pmm_map_unset(block_number);
        //kprintf("[PMM] Freed block no.: %d\n", block_number);
        pmm_blocks_used--;
        return true;
    }
}

//Returns block no. of the first free block
uint32_t pmm_map_first_free() {

    for (uint32_t i = 0; i < pmm_blocks_max / 32; i++) {

        if (pmm_memory_map[i] != 0xffffffff) {

            for (int j = 0; j < 32; j++) {

                uint32_t bit = 1 << j;

                if (! (pmm_memory_map[i] & bit)) {

                    kprintf("[PMM] First free: %d\n", i*32+j);
                    return i*32 + j;
                }
            }
        }
    }
    return -1;
}

//Frees multiple blocks at once
void pmm_free_region(uint32_t base, uint32_t blocks) {
    while (blocks > 0) {
        pmm_map_free_block(base++);
        blocks--;
    }
}

//Allocates multiple blocks at once
void pmm_alloc_region(uint32_t base, uint32_t blocks) {
    while (blocks > 0) {
        pmm_map_allocate_block(base++);
        blocks--;
    }
}
