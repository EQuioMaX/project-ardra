#include <kernel.h>

void kernelMain() {
    uint32_t blocks = pmm_init();
    kprintf("Block Count: %d\n", blocks);
    paging_init(blocks);
    kprintf("Welcome to project Ardra!\n");
    while (1);
}
