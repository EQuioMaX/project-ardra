#ifndef _KERNEL_H
#define _KERNEL_H

#include <arch/x86_64/bootstrap/multiboot.h>
#include <vga/kprintf.h>
#include <arch/x86/paging.h>
#include <mm/pmm.h>

void kernelMain();
void multiboot_parse(uint32_t magic, uint64_t mbi);

#endif