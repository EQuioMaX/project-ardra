#ifndef _PIC_H
#define _PIC_H

#include <asm.h>

static const uint16_t PIC_master = 0x20;
static const uint16_t PIC_slave = 0xA0;
static const uint16_t PIC_master_data = PIC_master + 1;
static const uint16_t PIC_slave_data = PIC_slave + 1;

void remapPIC (uint8_t offset1, uint8_t offset2);
void PIC_sendEOI (uint8_t irq);

#endif
