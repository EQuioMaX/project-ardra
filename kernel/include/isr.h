#ifndef _ISR_H
#define _ISR_H

#include <pic.h>
#include <vga/kprintf.h>

extern void keyboard_wrapper(void);
extern void dummy_wrapper(void);
extern void page_fault_wrapper(void);
extern void divide_by_zero_wrapper(void);
extern void debug_wrapper(void);
extern void nmi_wrapper(void);
extern void breakpoint_wrapper(void);
extern void overflow_wrapper(void);
extern void bound_range_wrapper(void);
extern void invalid_opcode_wrapper(void);
extern void no_device_wrapper(void);
extern void double_fault_wrapper(void);
extern void cop_segment_overrun_wrapper(void);
extern void invalid_tss_wrapper(void);
extern void no_segment_wrapper(void);
extern void ss_fault_wrapper(void);
extern void gpf_wrapper(void);
extern void fp_wrapper(void);
extern void alignment_check_wrapper(void);
extern void machine_check_wrapper(void);
extern void SIMD_wrapper(void);
extern void virt_wrapper(void);
extern void security_wrapper(void);

void kexcept_code(uint64_t cs, uint64_t ip, uint32_t code);
void kexcept(uint64_t cs, uint64_t ip);

void page_fault_handler (uint64_t cs, uint64_t ip, uint32_t code);
void divide_by_zero_handler(uint64_t cs, uint64_t ip);
void debug_handler(uint64_t cs, uint64_t ip);
void nmi_handler(uint64_t cs, uint64_t ip);
void breakpoint_handler(uint64_t cs, uint64_t ip);
void overflow_handler(uint64_t cs, uint64_t ip);
void bound_range_handler(uint64_t cs, uint64_t ip);
void invalid_opcode_handler(uint64_t cs, uint64_t ip);
void no_device_handler(uint64_t cs, uint64_t ip);
void double_fault_handler(uint64_t cs, uint64_t ip, uint32_t code);
void cop_segment_overrun_handler(uint64_t cs, uint64_t ip);
void invalid_tss_handler(uint64_t cs, uint64_t ip, uint32_t code);
void no_segment_handler(uint64_t cs, uint64_t ip, uint32_t code);
void ss_fault_handler(uint64_t cs, uint64_t ip, uint32_t code);
void gpf_handler(uint64_t cs, uint64_t ip, uint32_t code);
void fp_handler(uint64_t cs, uint64_t ip);
void alignment_check_handler(uint64_t cs, uint64_t ip, uint32_t code);
void machine_check_handler(uint64_t cs, uint64_t ip);
void SIMD_handler(uint64_t cs, uint64_t ip);
void virt_handler(uint64_t cs, uint64_t ip);
void security_handler(uint64_t cs, uint64_t ip, uint32_t code);
void dummy_handler(uint64_t cs, uint64_t ip);

#endif