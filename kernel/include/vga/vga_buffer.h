#ifndef _VGA_H
#define _VGA_H

#include <types.h>
#include <asm.h>

enum vga_colour {
	BLACK = 0,
	BLUE = 1,
	GREEN = 2,
	CYAN = 3,
	RED = 4,
	MAGENTA = 5,
	BROWN = 6,
	LIGHT_GREY = 7,
	DARK_GREY = 8,
	LIGHT_BLUE = 9,
	LIGHT_GREEN = 10,
	LIGHT_CYAN = 11,
	LIGHT_RED = 12,
	LIGHT_MAGENTA = 13,
	LIGHT_BROWN = 14,
	WHITE = 15,
};
 
static inline uint8_t vga_entry_colour (enum vga_colour fg, enum vga_colour bg);
static inline uint16_t vga_entry (uint8_t uc, uint8_t colour);
uint32_t strlen (const char* str);

static const uint32_t VGA_WIDTH = 80;
static const uint32_t VGA_HEIGHT = 25;
static uint16_t* const VGA_MEMORY = (uint16_t*) 0xB8000;
 
static uint32_t terminal_row;
static uint32_t terminal_column;
static uint8_t terminal_colour;
static uint16_t* terminal_buffer;
 
void terminal_initialize (void);
void terminal_setcolour (uint8_t colour);
void terminal_putentryat (uint8_t c, uint8_t colour, uint32_t x, uint32_t y);
void terminal_putchar (char c);
void terminal_write (const char* data, uint32_t size);
void terminal_writestring (const char* data);
void terminal_copy(int32_t dest_x, int32_t dest_y, int32_t src_x, int32_t src_y);
void clear();
void terminal_scroll();

#endif