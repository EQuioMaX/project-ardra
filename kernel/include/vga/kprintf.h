#ifndef _kPRINTF_H_
#define _kPRINTF_H_

#include <stdarg.h>
#include <stddef.h>
#include <vga/vga_buffer.h>


#define kprintf kprintf_
int kprintf_(const char* format, ...);

#define skprintf skprintf_
int skprintf_(char* buffer, const char* format, ...);

#define snkprintf  snkprintf_
#define vsnkprintf vsnkprintf_
int  snkprintf_(char* buffer, size_t count, const char* format, ...);
int vsnkprintf_(char* buffer, size_t count, const char* format, va_list va);

#define vkprintf vkprintf_
int vkprintf_(const char* format, va_list va);

int fctkprintf(void (*out)(char character, void* arg), void* arg, const char* format, ...);

#endif  // _kPRINTF_H_
