#ifndef _KBD_H
#define _KBD_H

#include <vga/vga_buffer.h>
#include <pic.h>
#include <drivers/kbd/keymap.h>

void keyboard_handler (void);

struct keyboard_states {
	uint32_t shift : 1;
	uint32_t alt   : 1;
	uint32_t ctrl  : 1;
} keyboard_state;

//typedef void (*kbd_handler_t)(int scancode);
void modifier_handler(uint8_t keycode);
void key_handler(uint8_t keycode);

#endif