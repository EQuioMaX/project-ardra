#ifndef PMM_H
#define PMM_H

#include <types.h>
#include <vga/kprintf.h>

static const uint32_t block_size = 4096;

extern uint32_t pmm_memory_size;
static uint32_t pmm_blocks_used = 0;
static uint32_t pmm_blocks_max = 0;

extern uint32_t pmm_mem_map;

extern void kernel_end;
extern void kernel_start;
static uint32_t *kernel_end_address = (uint32_t*)&kernel_end;
static uint32_t *kernel_start_address = (uint32_t*)&kernel_start;

static uint32_t *pmm_memory_map = 0;

static uint32_t frame_placement_address = 0;

typedef struct mem_map_entry {
    uint64_t address;
    uint64_t length;
    uint32_t type;
} mem_map_entry_t;

extern mem_map_entry_t mem_map[20];

static inline void pmm_map_set(int bit) {
    pmm_memory_map[bit/32] |= (1 << (bit % 32));
}

static inline void pmm_map_unset(int bit) {
    pmm_memory_map[bit/32] &= ~ (1 << (bit % 32));
}

static inline bool pmm_map_test(int bit) {
    return pmm_memory_map[bit/32] & (1 << (bit % 32));
}

bool pmm_map_allocate_block(uint32_t block_number);
bool pmm_map_free_block(uint32_t block_number);

uint32_t pmm_map_first_free(void);
uint32_t pmm_init(void);
void pmm_free_region(uint32_t base, uint32_t blocks);
void pmm_alloc_region(uint32_t base, uint32_t blocks);

#endif
