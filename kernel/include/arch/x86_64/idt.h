#ifndef _IDT_H
#define _IDT_H
#define KPRINTF_SUPPORT_LONG_LONG
#include <types.h>
#include <pic.h>
#include <asm.h>
#include <isr.h>

struct IDT_descriptor {
    uint16_t offset_0;
    uint16_t selector;
    uint8_t ist;
    uint8_t type_attr;
    uint16_t offset_1;
    uint32_t offset_2;
    uint32_t zero;
}__attribute__((packed));

struct IDT_pointer {
    uint16_t limit;
    uint64_t base;
}__attribute__((packed));

extern struct IDT_descriptor IDT[64];
extern struct IDT_pointer IDT_ptr;

extern void IDT_load();

void IDT_set_descriptor (uint8_t index, void (*int_handler)(void), uint8_t flags);
void IDT_set (void);
void idt_init (void);

#endif