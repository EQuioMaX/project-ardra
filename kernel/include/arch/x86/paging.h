#ifndef PAGING_H
#define PAGING_H

#include <vga/kprintf.h>

static uint64_t page_table[512] __attribute__((aligned(4096)));
static uint64_t page_dir[512] __attribute__((aligned(4096)));
static uint64_t page_dir_ptr_table[4] __attribute__((aligned(4096)));

void paging_init(uint32_t memsize);

void loadpagedir(uint64_t pd[]);
void paging_enable();

#endif