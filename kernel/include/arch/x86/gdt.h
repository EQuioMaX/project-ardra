#ifndef _GDT_H
#define _GDT_H

#include <types.h>

struct GDT_descriptor {
    uint16_t limit_0;
    uint16_t base_0;
    uint8_t base_1;
    uint8_t access;
    uint8_t limit_gran;
    uint8_t base_2;

}__attribute__((packed));

struct GDT_pointer {
    uint16_t limit;
    uint32_t base;
}__attribute__((packed));

extern struct GDT_descriptor GDT[5];
extern struct GDT_pointer GDT_ptr;

void GDT_set_descriptor(uint32_t index, uint32_t base, uint32_t limit, uint8_t access, uint8_t gran);
extern void GDT_flush();

void GDT_set();

#endif