global paging_set
global pagemap
section .text
bits 32

paging_set:
    call page_tables_set
    call paging_init
    ret

page_tables_set:
    xor eax, eax
    mov edi, pagemap
    mov ecx, (pagemap.end - pagemap)/4
    rep stosd

    mov eax, 0x03
    mov edi, pagemap.pt
    mov ecx, 512*32

    .loop0:
        stosd
        push eax
        xor eax, eax
        stosd
        pop eax
        add eax, 0x1000
        loop .loop0

    mov eax, pagemap.pt
    or eax, 0x03
    mov edi, pagemap.pd
    mov ecx, 32

    .loop1:
        stosd
        push eax
        xor eax, eax
        stosd
        pop eax
        add eax, 0x1000
        loop .loop1

    mov eax, pagemap.pd
    or eax, 0x03
    mov edi, pagemap.pdpt
    stosd
    xor eax, eax
    stosd

    mov eax, pagemap.pdpt
    or eax, 0x03
    mov edi, pagemap.pml4
    stosd
    xor eax, eax
    stosd

paging_init:
    mov eax, pagemap
    mov cr3, eax
    
    mov eax, cr4
    or eax, 1 << 5
    mov cr4, eax

    mov ecx, 0xC0000080
    rdmsr
    or eax, 1 << 8
    wrmsr

    mov eax, cr0
    or eax, 1 << 31
    mov cr0, eax
    ret

section .bss
align 4096

pagemap:
.pml4:
    resb 4096
.pdpt:
    resb 4096
.pd:
    resb 4096
.pt:
    resb 4096 * 32
.end:
