global IDT_load
extern IDT_ptr

global keyboard_wrapper
global dummy_wrapper
global page_fault_wrapper
global divide_by_zero_wrapper
global debug_wrapper
global nmi_wrapper
global breakpoint_wrapper
global overflow_wrapper
global bound_range_wrapper
global invalid_opcode_wrapper
global no_device_wrapper
global double_fault_wrapper
global cop_segment_overrun_wrapper
global invalid_tss_wrapper
global no_segment_wrapper
global ss_fault_wrapper
global gpf_wrapper
global fp_wrapper
global alignment_check_wrapper
global machine_check_wrapper
global SIMD_wrapper
global virt_wrapper
global security_wrapper

extern keyboard_handler
extern dummy_handler
extern page_fault_handler
extern divide_by_zero_handler
extern debug_handler
extern nmi_handler
extern breakpoint_handler
extern overflow_handler
extern bound_range_handler
extern invalid_opcode_handler
extern no_device_handler
extern double_fault_handler
extern cop_segment_overrun_handler
extern invalid_tss_handler
extern no_segment_handler
extern ss_fault_handler
extern gpf_handler
extern fp_handler
extern alignment_check_handler
extern machine_check_handler
extern SIMD_handler
extern virt_handler
extern security_handler

IDT_load:
	lidt [IDT_ptr]
	sti
	ret

%macro pushall 0
    push rax
    push rbx
    push rcx
    push rdx
    push rsi
    push rdi
    push rbp
    push r8
    push r9
    push r10
    push r11
    push r12
    push r13
    push r14
    push r15
%endmacro

%macro popall 0
    pop r15
    pop r14
    pop r13
    pop r12
    pop r11
    pop r10
    pop r9
    pop r8
    pop rbp
    pop rdi
    pop rsi
    pop rdx
    pop rcx
    pop rbx
    pop rax
%endmacro

%macro common_handler 1
    pushall
    call %1
    popall
    iretq
%endmacro

%macro except_handler 1
    pop rsi
    pop rdi
    call %1
    iretq
%endmacro

%macro except_handler_code 1
    pop rsi
    pop rdi
    pop rdx
    call %1
    iretq
%endmacro

keyboard_wrapper:
    common_handler keyboard_handler

dummy_wrapper:
    common_handler dummy_handler

page_fault_wrapper:
    except_handler_code page_fault_handler

divide_by_zero_wrapper:
    except_handler divide_by_zero_handler

debug_wrapper:
    except_handler debug_handler

nmi_wrapper:
    except_handler nmi_handler

breakpoint_wrapper:
    except_handler breakpoint_handler

overflow_wrapper:
    except_handler overflow_handler

bound_range_wrapper:
    except_handler bound_range_handler

invalid_opcode_wrapper:
    except_handler invalid_opcode_handler

no_device_wrapper:
    except_handler no_device_handler

double_fault_wrapper:
    except_handler double_fault_handler

cop_segment_overrun_wrapper:
    except_handler cop_segment_overrun_handler

invalid_tss_wrapper:
    except_handler_code invalid_tss_handler

no_segment_wrapper:
    except_handler_code no_segment_handler

ss_fault_wrapper:
    except_handler_code ss_fault_handler

gpf_wrapper:
    except_handler_code gpf_handler

fp_wrapper:
    except_handler fp_handler

alignment_check_wrapper:
    except_handler_code alignment_check_handler

machine_check_wrapper:
    except_handler machine_check_handler

SIMD_wrapper:
    except_handler SIMD_handler

virt_wrapper:
    except_handler virt_handler

security_wrapper:
    except_handler_code security_handler
