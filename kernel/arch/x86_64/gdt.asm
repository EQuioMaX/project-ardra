global gdt_ptr

section .data

align 16

gdt_ptr:
    dw GDT_end - GDT_start - 1
    dd GDT_start

align 16

GDT_start:

    Null_descriptor:
        dq 0

    kernel_64_code:
        dw 0x0FFFF
        dw 0x0000
        db 0x00
        db 0x9A
        db 0xAF
        db 0x00

    kernel_data:
        dw 0x0FFFF
        dw 0x0000
        db 0x00
        db 0x92
        db 0xAF
        db 0x00
    
GDT_end:
