global boot
global GDT_flush

extern gdt_ptr
extern kernelboot
extern paging_set

MAGIC_NUMBER equ 0xE85250D6
CHECKSUM equ -(MAGIC_NUMBER + SIZE)
MBFLAGS equ 0x0
SIZE equ header_end - header

section .multiboot

header:
align 8
dd MAGIC_NUMBER
dd 0 ;i386
dd SIZE
dd CHECKSUM
header_end:

section .data
align 8
multiboot dd 0
multiboot_magic dd 0

section .text
bits 32

boot:
    cli
    mov esp, stack_top
    mov [multiboot], ebx
    mov [multiboot_magic], eax
    call paging_set
    lgdt [gdt_ptr]

    jmp 0x08:long_init

long_init:
bits 64
    mov ax, 0x10
    mov ss, ax
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    xor rdi, rdi
    xor rsi, rsi
    mov rdi, qword [multiboot_magic]
    mov rsi, qword [multiboot]
    call kernelboot
    jmp hang

hang:
    cli
    hlt
    jmp hang

section .bss
stack_bottom:
    resb 16384
stack_top: