#include <arch/x86/gdt.h>
#include <arch/x86/idt.h>
#include <kernel.h>
#define CHECK_FLAG(flags, bit)  ((flags) & (1<<(bit)))

void kernelboot(uint32_t magic,uint64_t mbi) {
    GDT_set();
    idt_init();
    terminal_initialize();
    multiboot_parse(magic, mbi);
    kernelMain();
    while(1);
}

void multiboot_parse(uint32_t magic, uint64_t mbi) {
    if(magic != MULTIBOOT2_BOOTLOADER_MAGIC) {
        kprintf("Invalid magic number: 0x%x\n", (unsigned)magic);
        while(1);
    }

    struct multiboot_tag *tag;
    unsigned size;
    if (mbi & 7) {
        kprintf("Unaligned MBI: 0x%x\n", mbi);
    }
    size = *(unsigned *)mbi;
    kprintf("Announced MBI size: 0x%x\n", mbi);
    for (tag = (struct multiboot_tag *)(mbi+8); tag->type != MULTIBOOT_TAG_TYPE_END; tag = (struct multiboot_tag *)((multiboot_uint8_t *)tag + ((tag->size + 7) & ~7))) {
        kprintf("[Tag 0x%x, Size 0x%x\n", tag->type, tag->size);

        switch (tag->type) {
        case MULTIBOOT_TAG_TYPE_CMDLINE:
            kprintf("Command line = %s]\n", ((struct multiboot_tag_string *) tag)->string);
            break;
        case MULTIBOOT_TAG_TYPE_BOOT_LOADER_NAME:
            kprintf("Boot loader name = %s]\n", ((struct multiboot_tag_string *) tag)->string);
            break;
        case MULTIBOOT_TAG_TYPE_MODULE:
            kprintf ("Module at 0x%x-0x%x. Command line %s]\n",
                    ((struct multiboot_tag_module *) tag)->mod_start,
                    ((struct multiboot_tag_module *) tag)->mod_end,
                    ((struct multiboot_tag_module *) tag)->cmdline);
            break;
        case MULTIBOOT_TAG_TYPE_BASIC_MEMINFO: {
            pmm_memory_size = ((struct multiboot_tag_basic_meminfo *) tag)->mem_upper;
            kprintf ("mem_lower = %uKB, mem_upper = %uKB]\n",
                    ((struct multiboot_tag_basic_meminfo *) tag)->mem_lower,
                    ((struct multiboot_tag_basic_meminfo *) tag)->mem_upper);
            
            break;
        }
        case MULTIBOOT_TAG_TYPE_BOOTDEV:
            kprintf ("Boot device 0x%x, %u, %u]\n",
                    ((struct multiboot_tag_bootdev *) tag)->biosdev,
                    ((struct multiboot_tag_bootdev *) tag)->slice,
                    ((struct multiboot_tag_bootdev *) tag)->part);
            break;
        case MULTIBOOT_TAG_TYPE_MMAP: {
            multiboot_memory_map_t *mmap;

            kprintf ("MMap\n");
            int i = 0;
            for (mmap = ((struct multiboot_tag_mmap *) tag)->entries; (multiboot_uint8_t *) mmap < (multiboot_uint8_t *) tag + tag->size; mmap = (multiboot_memory_map_t *)((unsigned long) mmap + ((struct multiboot_tag_mmap *) tag)->entry_size)) {
                kprintf ("base_addr = 0x%x%x, length = 0x%x%x, type = 0x%x\n", (unsigned) (mmap->addr >> 32), (unsigned) (mmap->addr & 0xffffffff), (unsigned) (mmap->len >> 32), (unsigned) (mmap->len & 0xffffffff), (unsigned) mmap->type);
                mem_map[i].address = mmap->addr;
                mem_map[i].length = mmap->len;
                mem_map[i].type = mmap->type;
                i++;
            }
          }
          break;
        }



    }
}
