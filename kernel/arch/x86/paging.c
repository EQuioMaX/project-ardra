#include <arch/x86/paging.h>

void paging_init(uint32_t memsize) {
    page_dir_ptr_table[0] = (uint64_t) &page_dir | 0x01;        // Mark as Present
    page_dir[0] = (uint64_t) &page_table | 0x03;
    
    unsigned int i, address = 0;
    for(i = 0; i < 512; i++) {
    page_table[i] = address | 3;                                // Map address and mark it present/writable
    address = address + 0x1000;
    //kprintf("[PG] Mapped page %d\n", i);
    }

    paging_enable();
}

void loadpagedir(uint64_t pdpt[]) {
    asm volatile ("movl %0, %%cr3" :: "r" (&pdpt)); // load PDPT into CR3
    kprintf("[PG] Loaded PDPT into CR3\n");
}

void paging_enable() {
    asm volatile ("movl %%cr4, %%eax; bts $5, %%eax; movl %%eax, %%cr4" ::: "eax"); // set bit5 in CR4 to enable PAE
    kprintf("[PG] Enabled PAE\n");
    asm volatile ("movl %0, %%cr3" :: "r" (&page_dir_ptr_table));
    asm volatile ("movl %%cr0, %%eax; orl $0x80000000, %%eax; movl %%eax, %%cr0;" ::: "eax");
    kprintf("[PG] Paging enabled\n");
}