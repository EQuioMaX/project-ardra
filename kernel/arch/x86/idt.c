#include <arch/x86/idt.h>

struct IDT_descriptor IDT[256];
struct IDT_pointer IDT_ptr;

void IDT_set_descriptor(uint8_t index, void (*int_handler)(void), uint8_t flags) {
    uint64_t offset = (uint64_t) int_handler;
    IDT[index].offset_0 = (offset & 0xFFFF);
    IDT[index].offset_1 = (offset >> 16) & 0xFFFF;
    IDT[index].zero = 0;
    IDT[index].selector = 0x08;
    IDT[index].type_attr = flags;
}

void IDT_set(void) {
    IDT_ptr.limit = (sizeof(struct IDT_descriptor) * 256) - 1;
    IDT_ptr.base = (uint32_t)IDT;
    IDT_load();
}

void idt_init(void) {
    remapPIC(0x20,0xA0);
    outb(PIC_master_data, 0xFD);
    for (uint16_t index = 0; index < 256; index++) {
        IDT_set_descriptor(index, dummy_wrapper, 0x8E);
    }

    IDT_set_descriptor(0x00, divide_by_zero_wrapper, 0x8E);
    IDT_set_descriptor(0x01, debug_wrapper, 0x8E);
    IDT_set_descriptor(0x02, nmi_wrapper, 0x8E);
    IDT_set_descriptor(0x03, breakpoint_wrapper, 0x8E);
    IDT_set_descriptor(0x04, overflow_wrapper, 0x8E);
    IDT_set_descriptor(0x05, bound_range_wrapper, 0x8E);
    IDT_set_descriptor(0x06, invalid_opcode_wrapper, 0x8E);
    IDT_set_descriptor(0x07, no_device_wrapper, 0x8E);
    IDT_set_descriptor(0x08, double_fault_wrapper, 0x8E);
    IDT_set_descriptor(0x09, cop_segment_overrun_wrapper, 0x8E);
    IDT_set_descriptor(0x0A, invalid_tss_wrapper, 0x8E);
    IDT_set_descriptor(0x0B, no_segment_wrapper, 0x8E);
    IDT_set_descriptor(0x0C, ss_fault_wrapper, 0x8E);
    IDT_set_descriptor(0x0D, gpf_wrapper, 0x8E);
    IDT_set_descriptor(0x0E, page_fault_wrapper, 0x8E);
    IDT_set_descriptor(0x0F, dummy_wrapper, 0x8E);
    IDT_set_descriptor(0x10, fp_wrapper, 0x8E);
    IDT_set_descriptor(0x11, alignment_check_wrapper, 0x8E);
    IDT_set_descriptor(0x12, machine_check_wrapper, 0x8E);
    IDT_set_descriptor(0x13, SIMD_wrapper, 0x8E);
    IDT_set_descriptor(0x14, virt_wrapper, 0x8E);
    //0x15 to 0x1D reserved
    IDT_set_descriptor(0x1E, security_wrapper, 0x8E);

    IDT_set_descriptor(0x21, keyboard_wrapper, 0x8E);
    IDT_set();
}
