#include <arch/x86/gdt.h>
struct GDT_descriptor GDT[5];
struct GDT_pointer GDT_ptr;

void GDT_set_descriptor(uint32_t index, uint32_t base, uint32_t limit, uint8_t access, uint8_t gran) {
    GDT[index].base_0 = (base & 0xFFFF);
    GDT[index].base_1 = (base >> 16) & 0xFF;
    GDT[index].base_2 = (base >> 24) & 0xFF;

    GDT[index].limit_0 = (limit & 0xFFFF);
    GDT[index].limit_gran = (limit >> 16) & 0x0F;
    GDT[index].limit_gran |= (gran & 0xF0);
    GDT[index].access = access;
}

void GDT_set() {
    GDT_ptr.limit = (sizeof(struct GDT_descriptor) * 5) - 1;
    GDT_ptr.base = (uint32_t)GDT;

    GDT_set_descriptor(0,0,0,0,0);
    GDT_set_descriptor(1,0,0xFFFFFFFF,0x9A,0xCF);
    GDT_set_descriptor(2,0,0xFFFFFFFF,0x92,0xCF);
    GDT_set_descriptor(3,0,0xFFFFFFFF,0xFA,0xCF);
    GDT_set_descriptor(4,0,0xFFFFFFFF,0xF2,0xCF);

    GDT_flush();
}