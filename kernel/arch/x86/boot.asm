[BITS 32]
global boot
global GDT_flush
global IDT_load
global pmm_mem_map

extern GDT_ptr
extern IDT_ptr

MAGIC_NUMBER equ 0xE85250D6
CHECKSUM equ -(MAGIC_NUMBER + SIZE)
MBFLAGS equ 0x0
SIZE equ header_end - header
;FB_SIZE equ framebuffer_tag_end - framebuffer_tag

section .multiboot
header:
align 8
dd MAGIC_NUMBER
dd 0 ;i386
dd SIZE
dd CHECKSUM
header_end:

section .text
boot:
	cli
	mov esp, _stack
	extern kernelboot
	push ebx
	push eax
	call kernelboot
	jmp hang

hang:
	cli
	hlt
	jmp hang

GDT_flush:
	lgdt [GDT_ptr]
	mov ax, 0x10
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax
	mov ss, ax
	jmp 0x08:flush2
	
flush2:
	ret

global stack_dump

stack_dump:
	push ebp
	mov ebp, esp
	pop ebp
	ret

section .bss
stack_bottom:
resb 16384
_stack:
align 4096
pmm_mem_map:
resb 131_072
