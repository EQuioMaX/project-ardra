global IDT_load
extern IDT_ptr

global keyboard_wrapper
global dummy_wrapper
global page_fault_wrapper
global divide_by_zero_wrapper
global debug_wrapper
global nmi_wrapper
global breakpoint_wrapper
global overflow_wrapper
global bound_range_wrapper
global invalid_opcode_wrapper
global no_device_wrapper
global double_fault_wrapper
global cop_segment_overrun_wrapper
global invalid_tss_wrapper
global no_segment_wrapper
global ss_fault_wrapper
global gpf_wrapper
global fp_wrapper
global alignment_check_wrapper
global machine_check_wrapper
global SIMD_wrapper
global virt_wrapper
global security_wrapper

extern keyboard_handler
extern dummy_handler
extern page_fault_handler
extern divide_by_zero_handler
extern debug_handler
extern nmi_handler
extern breakpoint_handler
extern overflow_handler
extern bound_range_handler
extern invalid_opcode_handler
extern no_device_handler
extern double_fault_handler
extern cop_segment_overrun_handler
extern invalid_tss_handler
extern no_segment_handler
extern ss_fault_handler
extern gpf_handler
extern fp_handler
extern alignment_check_handler
extern machine_check_handler
extern SIMD_handler
extern virt_handler
extern security_handler

IDT_load:
	lidt [IDT_ptr]
	sti
	ret

%macro pushall 0
    pushad
    push ds
    push es
    push fs
    push gs
%endmacro

%macro popall 0
    pop gs
    pop fs
    pop es
    pop ds
    popad
%endmacro

%macro call_handler 1
    pushall
    call %1
    popall
    iret
%endmacro

keyboard_wrapper:
    call_handler keyboard_handler

dummy_wrapper:
    call_handler dummy_handler

page_fault_wrapper:
    call_handler page_fault_handler

divide_by_zero_wrapper:
    call_handler divide_by_zero_handler

debug_wrapper:
    call_handler debug_handler

nmi_wrapper:
    call_handler nmi_handler

breakpoint_wrapper:
    call_handler breakpoint_handler

overflow_wrapper:
    call_handler overflow_handler

bound_range_wrapper:
    call_handler bound_range_handler

invalid_opcode_wrapper:
    call_handler invalid_opcode_handler

no_device_wrapper:
    call_handler no_device_handler

double_fault_wrapper:
    call_handler double_fault_handler

cop_segment_overrun_wrapper:
    call_handler cop_segment_overrun_handler

invalid_tss_wrapper:
    call_handler invalid_tss_handler

no_segment_wrapper:
    call_handler no_segment_handler

ss_fault_wrapper:
    call_handler ss_fault_handler

gpf_wrapper:
    call_handler gpf_handler

fp_wrapper:
    call_handler fp_handler

alignment_check_wrapper:
    call_handler alignment_check_handler

machine_check_wrapper:
    call_handler machine_check_handler

SIMD_wrapper:
    call_handler SIMD_handler

virt_wrapper:
    call_handler virt_handler

security_wrapper:
    call_handler security_handler
