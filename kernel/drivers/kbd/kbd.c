#include <drivers/kbd/kbd.h>

void keyboard_handler(void) {
    uint8_t status;
    uint8_t keycode;

    PIC_sendEOI(1);
    status = inb(0x64);

    //if (status & 0x01) {
        keycode = inb(0x60);
        modifier_handler(keycode);
    //}
}

void modifier_handler(uint8_t keycode) {
    switch (keycode) {

        case 0x1D: {
            keyboard_state.ctrl |=1;
            break;
        }

        case 0x9D: {
            keyboard_state.ctrl &=0;
            break;
        }

        case 0x2A: {
            keyboard_state.shift |=1;
            break;
        }

        case 0x36: {
            keyboard_state.shift |=1;
            break;
        }

        case 0xAA: {
            keyboard_state.shift &=0;
            break;
        }

        case 0xB6: {
            keyboard_state.shift &=0;
            break;
        }

        case 0x38: {
            keyboard_state.alt^=1;
            break;
        }
        
        default: {
            if (keycode & 0x80){
                break;
            }
            key_handler(keycode);
            break;
        }
    }
}

void key_handler(uint8_t keycode) {
    if(keyboard_state.shift) {
            terminal_putchar(keymap_shift[keycode]);
        }
        else if(keyboard_state.ctrl) {
            terminal_putchar('^');
            terminal_putchar(keymap_shift[keycode]);
        }
        else {
            terminal_putchar(keymap[keycode]);
        }
}