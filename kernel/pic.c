#include <pic.h>

void remapPIC (uint8_t offset1, uint8_t offset2) {
    uint8_t master_mask, slave_mask;

    master_mask = inb(PIC_master_data);
    slave_mask = inb(PIC_slave_data);

    outb(PIC_master, 0x11);
    io_wait();
    outb(PIC_slave, 0x11);
    io_wait();

    outb(PIC_master_data, offset1);
    io_wait();
    outb(PIC_slave_data, offset2);
    io_wait();
    outb(PIC_master_data, 0x4);
    io_wait();
    outb(PIC_slave_data, 0x2);
    io_wait();

    outb(PIC_master_data, 0x01);
	io_wait();
	outb(PIC_slave_data, 0x01);
	io_wait();

    outb(PIC_master_data, master_mask);
    outb(PIC_slave_data, slave_mask);

}

void PIC_sendEOI (uint8_t irq) {
    if(irq >= 8) {
        outb(PIC_slave, 0x20);
    }
    outb(PIC_master, 0x20);
}
