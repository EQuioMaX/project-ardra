#include <vga/vga_buffer.h>

static inline uint8_t vga_entry_colour (enum vga_colour fg, enum vga_colour bg) {
	return fg | bg << 4;
}
 
static inline uint16_t vga_entry (uint8_t uc, uint8_t colour) {
	return (uint16_t) uc | (uint16_t) colour << 8;
}

uint32_t strlen (const char* str) {
	uint32_t len = 0;
	while (str[len])
		len++;
	return len;
}

void terminal_initialize (void) {
	terminal_row = 0;
	terminal_column = 0;
	terminal_colour = vga_entry_colour(BLACK, WHITE);
	terminal_buffer = VGA_MEMORY;
	for (uint32_t y = 0; y < VGA_HEIGHT; y++) {
		for (uint32_t x = 0; x < VGA_WIDTH; x++) {
			const uint32_t index = y * VGA_WIDTH + x;
			terminal_buffer[index] = vga_entry(' ', terminal_colour);
		}
	}
	disable_cursor();
}

void terminal_setcolour (uint8_t colour) {
	terminal_colour = colour;
}
 
void terminal_putentryat (uint8_t c, uint8_t colour, uint32_t x, uint32_t y) {
	const uint32_t index = y * VGA_WIDTH + x;
	terminal_buffer[index] = vga_entry(c, colour);
}
 
void terminal_putchar(char c) {
	uint8_t uc = c;
	switch (uc) {

		case '\n': {
			if (terminal_row + 1 >= VGA_HEIGHT) {
				terminal_scroll();
				terminal_column = 0;
			}
			else
				terminal_row++;
			terminal_column=0;
			break;
		}

		case '\t': {
			terminal_column+=4;
			break;
		}

		case '\b' : {
			--terminal_column;
			terminal_putentryat(' ', terminal_colour, terminal_column, terminal_row);
			break;
		}

		default:
		{
			terminal_putentryat(uc, terminal_colour, terminal_column, terminal_row);
			if (++terminal_column == VGA_WIDTH) {
				terminal_column = 0;
				if (terminal_row + 1 == VGA_HEIGHT) {
					terminal_scroll();
					terminal_column = 0;
				}
			}
		}
	}
	
}

//From Cppdev branch

void terminal_copy(int32_t dest_x, int32_t dest_y, int32_t src_x, int32_t src_y) {
    terminal_buffer[(dest_y * VGA_WIDTH + dest_x)] = terminal_buffer[(src_y * VGA_WIDTH + src_x)];
}

void clear() {
    for (int y = 0; y < VGA_HEIGHT; y++) {
        
        for (int x = 0; x < VGA_WIDTH; x++) {
            
            terminal_putentryat(' ', terminal_colour,x, y);
        }
    }
}

void terminal_scroll() {
    for (int y = 0; y < VGA_HEIGHT - 1; y++) {
        
        for (int x = 0; x < VGA_WIDTH; x++) {
            
            terminal_copy(x, y, x, y + 1);
        }
    }

    for (int x = 0; x < VGA_WIDTH; x++) {
        terminal_putentryat(' ', terminal_colour, x, VGA_HEIGHT - 1);
	}
	
	terminal_column = 0;
	terminal_row = VGA_HEIGHT - 1;
}