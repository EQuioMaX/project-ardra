#include <isr.h>

void page_fault_handler(uint64_t cs, uint64_t ip, uint32_t code) {
    uint64_t fault_address;
    asm volatile ("mov %%cr2, %0": "=r"(fault_address));
    kprintf("Exception: Page Fault!\nFaulting Address: 0x%x\n", fault_address);
    kexcept_code(cs, ip, code);
}

void divide_by_zero_handler(uint64_t cs, uint64_t ip) {
    kprintf("Exception: Divide by Zero!\n");
    kexcept(cs, ip);
    while(1);
}

void debug_handler(uint64_t cs, uint64_t ip) {
    kprintf("Exception: Debug Exception!\n");
    kexcept(cs, ip);
    while(1);
}

void nmi_handler(uint64_t cs, uint64_t ip) {
    kprintf("Exception: Non-maskable Interrupt!\n");
    kexcept(cs, ip);
    while(1);
}

void breakpoint_handler(uint64_t cs, uint64_t ip) {
    kprintf("Exception: Breakpoint\n");
    kexcept(cs, ip);
    while(1);
}

void overflow_handler(uint64_t cs, uint64_t ip) {
    kprintf("Exception: Overflow\n");
    kexcept(cs, ip);
    while(1);
}

void bound_range_handler(uint64_t cs, uint64_t ip) {
    kprintf("Exception: Bound range exceeded!\n");
    kexcept(cs, ip);
    while(1);
}

void invalid_opcode_handler(uint64_t cs, uint64_t ip) {
    kprintf("Exception: Invalid Opcode!\n");
    kexcept(cs, ip);
    while(1);
}

void no_device_handler(uint64_t cs, uint64_t ip) {
    kprintf("Exception: Device not found!\n");
    kexcept(cs, ip);
    while(1);
}

void double_fault_handler(uint64_t cs, uint64_t ip, uint32_t code) {
    kprintf("Exception: Double Fault!\n");
    kexcept_code(cs, ip, code);
}

void cop_segment_overrun_handler(uint64_t cs, uint64_t ip) {
    kprintf("Exception: Coprocessor Segment Overrun!\n");
    kexcept(cs, ip);
    while(1);
}

void invalid_tss_handler(uint64_t cs, uint64_t ip, uint32_t code) {
    kprintf("Exception: Invalid TSS!\n");
    kexcept_code(cs, ip, code);
}

void no_segment_handler(uint64_t cs, uint64_t ip, uint32_t code) {
    kprintf("Exception: Segment not found!\n");
    kexcept_code(cs, ip, code);
}

void ss_fault_handler(uint64_t cs, uint64_t ip, uint32_t code) {
    kprintf("Exception: Stack Segment fault!\n");
    kexcept_code(cs, ip, code);
}

void gpf_handler(uint64_t cs, uint64_t ip, uint32_t code) {
    kprintf("Exception: General Protection Fault!\n");
    kexcept_code(cs, ip, code);
    while(1);
}

void fp_handler(uint64_t cs, uint64_t ip) {
    kprintf("Exception: x87 floating-point exception!\n");
    kexcept(cs, ip);
    while(1);
}

void alignment_check_handler(uint64_t cs, uint64_t ip, uint32_t code) {
    kprintf("Exception: Alignment Check!\n");
    kexcept_code(cs, ip, code);
}

void machine_check_handler(uint64_t cs, uint64_t ip) {
    kprintf("Exception: Machine Check!\n");
    kexcept(cs, ip);
    while(1);
}

void SIMD_handler(uint64_t cs, uint64_t ip) {
    kprintf("Exception: SIMD Exception!\n");
    kexcept(cs, ip);
    while(1);
}

void virt_handler(uint64_t cs, uint64_t ip) {
    kprintf("Exception: Virtualisation Exception!\n");
    kexcept(cs, ip);
    while(1);
}

void security_handler(uint64_t cs, uint64_t ip, uint32_t code) {
    kprintf("Exception: Security Exception!\n");
    kexcept_code(cs, ip, code);
    while(1);
}

void dummy_handler(uint64_t cs, uint64_t ip) {
    kprintf("Unhandled Interrupt!\n");
    kexcept(cs, ip);
    while(1);
}

void kexcept_code(uint64_t cs, uint64_t ip, uint32_t code) {
    kprintf("Offending instruction's Address:\nCS: 0x%x\tIP: 0x%x\tCode: 0x%x\n", cs, ip, code);
}

void kexcept(uint64_t cs, uint64_t ip) {
    kprintf("Offending instruction's Address:\nCS: 0x%x\tIP: 0x%x\n", cs, ip);
}
